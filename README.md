# Differential Expression

This folder containts everything related to the publication of the paper entitled: "On potential limitations of differential expression analysis with non-linear machine learning models".

## Getting started

The following R Packages have to be installed for proper execution of the scripts: 
- data.table
- ggplot2
- gridExtra
- DESeq2
- edgeR
- cowplot

The following python Packages have to be installed for proper execution of the scripts:
- shap
- pandas
- numpy
- matplotlib
- lime
- warnings
- sklearn

If the loading/installation of these packages shows some problems, the code will not run.

## Folders

- data : contains the dataset used in the publication. These are generated using the scripts described below.

## Files

The directory contains the following files: 
- generate-dataset_functions.R : contains the functions needed to create the dataset
- generate-dataset.R : script to create the 3 datasets (+1 supplementary), exploiting the functions defined in the generate-dataset_functions.R script
- plot-dataset.R : script to plot the datasets (Figure 1 of the paper + Supplementary Figures 1-3)
- compute_DE.R : script to compute and plot differential expression analysis (Figure 2 of the paper + Supplementary Figure 4)
- XAI - clouds.ipynb : script to compute XAI-based feature importance of dataset A (clouds)
- XAI - circles.ipynb : script to compute XAI-based feature importance of dataset B (circles)
- XAI - big_circles.ipynb : script to compute XAI-based feature importance of dataset C (circles - big)
- XAI - bigcircles_neg-binomial.ipynb: script to compute XAI-based feature importance of dataset D (big circles with negative binomial distribution)
- plot_XAI_results.R : script to plot the results of XAI-based feature importance analysis (Figure 3 of the paper + Supplementary Figure 5)

## Usage

Run the scripts in the order they are presented in the Files section.

## Authors and acknowledgment

Work has been performed by aizoOn Technology consulting as part of project DEFLeCT (Digital tEChnology For Lung Cancer Treatment).

<img src="https://aizoongroup.com/Style%20Library/images/logo.png" width="350">

## License

This work is licensed under the Gnu Affero Public License V3.0 (see https://gitlab.com/deflect-public/consensus-clustering/-/blob/main/LICENSE)

<img src="https://www.regione.piemonte.it/loghi/im/grafica/piede_fesr.jpg">
