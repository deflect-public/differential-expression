

##################################### CLEAN ENVIRONMENT #####################################
#
rm(list=ls())



######################################## LIBRARIES ########################################
#
message('Loading libraries...')

library(data.table)
setDTthreads(threads = 6)

library(ggplot2)
theme_set(theme_bw(14) + theme(axis.text = element_text(colour="black")))

library(gridExtra)



######################################## PATHs ########################################
#
message(paste0('Defining PATHs...'))

PATH.data <- './data/'

PATH.plots <- './plots/'
if(!dir.exists(PATH.plots)) {
  dir.create(PATH.plots, recursive = T)
}



######################################## LOAD DATASET ########################################
#
message(paste0('Loading datasets...'))

message(paste0(' - clouds'))
cloud <- fread(input = paste0(PATH.data, 'dataset_clouds.csv'))

message(paste0(' - circles'))
circles <- fread(input = paste0(PATH.data, 'dataset_circles.csv'))

message(paste0(' - big circles'))
big.circles <- fread(input = paste0(PATH.data, 'dataset_big-circles.csv'))
map_f_x <- fread(input = paste0(PATH.data, 'big-circles__map_f_x.csv'), header = T)
n_relevant <- 2
relevant.x <- map_f_x[f%in%paste0('f',ints_to_n_char(v = 1:n_relevant, n_ = 4))][order(f)]$x

message(paste0(' - binomial'))
n_relevant_vars <- 200
binomial <- fread(input = paste0(PATH.data, 'dataset_big-circles-binomial__',n_relevant_vars,'-vars.csv'))




######################################## SUPPLEMENTARY PLOTS ########################################
#
# clouds
dt.plot <- melt.data.table(data = cloud, id.vars = 'cluster')
p2 <- ggplot(data = dt.plot[variable%in%c('x9','x20'),]) +
  geom_histogram(mapping = aes(x = value, colour = variable, fill = variable, group = variable), alpha = 0.5, bins = 50, position = 'identity') +
  scale_x_continuous(name = 'synthetic expression values') +
  scale_y_continuous(name = 'counts')

p1 <- ggplot(data = dt.plot) +
  geom_histogram(mapping = aes(x = value), boundary = 0, binwidth = 300, position = 'identity', colour = 'black', fill = 'grey60', ) +
  scale_x_continuous(name = 'synthetic expression values') +
  scale_y_continuous(name = 'counts')

p3 <- ggplot(data = dt.plot[variable%in%c('x9','x20'),]) +
  geom_boxplot(mapping = aes(x = variable, y = value, colour = cluster, group = interaction(variable,cluster)), outlier.alpha = 1) +
  scale_y_continuous(name = 'synthetic expression values')

p.up <- cowplot::plot_grid(p1,p2, labels = c('A','B'), label_size = 12, nrow = 1)
p.all <- cowplot::plot_grid(p.up,p3, labels = c('', 'C'), label_size = 12, nrow = 2)
ggsave(plot = p.all, filename = paste0(PATH.plots, 'clouds_combined.png'), width = 12, height = 12, dpi = 1000)


# circles
dt.plot <- melt.data.table(data = circles, id.vars = 'cluster')
p2 <- ggplot(data = dt.plot[variable%in%c('x4','x17'),]) +
  geom_histogram(mapping = aes(x = value, colour = variable, fill = variable, group = variable), alpha = 0.5, bins = 50, position = 'identity') +
  scale_x_continuous(name = 'synthetic expression values') +
  scale_y_continuous(name = 'counts')

p1 <- ggplot(data = dt.plot) +
  geom_histogram(mapping = aes(x = value), boundary = 0, binwidth = 300, position = 'identity', colour = 'black', fill = 'grey60', ) +
  scale_x_continuous(name = 'synthetic expression values') +
  scale_y_continuous(name = 'counts')

p3 <- ggplot(data = dt.plot[variable%in%c('x4','x17'),]) +
  geom_boxplot(mapping = aes(x = variable, y = value, colour = cluster, group = interaction(variable,cluster)), outlier.alpha = 1) +
  scale_y_continuous(name = 'synthetic expression values')

p.up <- cowplot::plot_grid(p1,p2, labels = c('A','B'), label_size = 12, nrow = 1)
p.all <- cowplot::plot_grid(p.up,p3, labels = c('', 'C'), label_size = 12, nrow = 2)
ggsave(plot = p.all, filename = paste0(PATH.plots, 'circles_combined.png'), width = 12, height = 12, dpi = 1000)


# circles (negative binomial)
p1 <- ggplot(data = dt.plot) +
  geom_histogram(mapping = aes(x = value), boundary = 0, binwidth = 1000, position = 'identity', colour = 'black', fill = 'grey60', ) +
  scale_x_continuous(name = 'synthetic expression values') +
  scale_y_continuous(name = 'counts')
ggsave(plot = p1, filename = paste0(PATH.plots, 'negative-binomial_histogram.png'), width = 10, height = 7, dpi = 1000)



######################################## PLOTS ########################################
#
# NOTE: relevant features
#   - clouds ========> x9,x20
#   - circles =======> x4,x17
#   - big ===========> x23,x83
warning(paste0('Please check that relevant features are x9,x20 - x4,x17 - x23,x83 for the 3 datasets respectively.'))

message(paste0('Computing metrics & plotting datasets...'))

message(paste0(' - clouds'))

m1.x <- mean(cloud[cluster=='cluster-01',]$x9)
m2.x <- mean(cloud[cluster=='cluster-02',]$x9)
sd1.x <- sd(cloud[cluster=='cluster-01',]$x9)
sd2.x <- sd(cloud[cluster=='cluster-02',]$x9)
print(paste0('CLOUDS - cluster-01: x = ',round(m1.x,2),' +- ',round(sd1.x,2)))
print(paste0('CLOUDS - cluster-02: x = ',round(m2.x,2),' +- ',round(sd2.x,2)))
z.x <- round(abs(m1.x-m2.x)/sqrt(sd1.x^2+sd2.x^2),2)
message(paste0('CLOUDS x - Z score: ',z.x))

m1.y <- mean(cloud[cluster=='cluster-01',]$x20)
m2.y <- mean(cloud[cluster=='cluster-02',]$x20)
sd1.y <- sd(cloud[cluster=='cluster-01',]$x20)
sd2.y <- sd(cloud[cluster=='cluster-02',]$x20)
print(paste0('CLOUDS - cluster-01: y = ',round(m1.y,2),' +- ',round(sd1.y,2)))
print(paste0('CLOUDS - cluster-02: y = ',round(m2.y,2),' +- ',round(sd2.y,2)))
z.y <- round(abs(m1.y-m2.y)/sqrt(sd1.y^2+sd2.y^2),2)
message(paste0('CLOUDS y - Z score: ',z.y))

media <- data.table(x = c(m1.x, m2.x), y = c(m1.y, m2.y), stdev.x = c(sd1.x, sd2.x), stdev.y = c(sd1.y, sd2.y), cluster = c('cluster-01', 'cluster-02'))

p.x <- round((1-pnorm(q = z.x))*2,3);p.x <- ifelse(p.x<0.001,'<0.001',as.character(p.x))
p.y <- round((1-pnorm(q = z.y))*2,3);p.y <- ifelse(p.y<0.001,'<0.001',as.character(p.y))

p.clouds <- ggplot() +
  geom_point(data = cloud, mapping = aes(x = x9, y = x20, colour = cluster, shape = cluster)) +
  geom_point(data = media[cluster=='cluster-01',], mapping = aes(x = x, y = y), colour = 'darkred', shape = 4, stroke = 1.7) +
  geom_point(data = media[cluster=='cluster-02',], mapping = aes(x = x, y = y), colour = 'deepskyblue4', shape = 4, stroke = 1.7) +
  geom_errorbar(data = media[cluster=='cluster-01',], mapping = aes(x = x, ymin = y-stdev.y, ymax = y+stdev.y), colour = 'darkred', width = 0.1) +
  geom_errorbar(data = media[cluster=='cluster-02',], mapping = aes(x = x, ymin = y-stdev.y, ymax = y+stdev.y), colour = 'deepskyblue4', width = 0.1) +
  geom_errorbarh(data = media[cluster=='cluster-01',], mapping = aes(xmin = x-stdev.x, xmax = x+stdev.x, y = y), colour = 'darkred', height = 0.1) +
  geom_errorbarh(data = media[cluster=='cluster-02',], mapping = aes(xmin = x-stdev.x, xmax = x+stdev.x, y = y), colour = 'deepskyblue4', height = 0.1) +
  coord_fixed() +
  scale_x_continuous(limits = c(5000,15000)) +
  scale_y_continuous(limits = c(5000,15000)) +
  theme(axis.text = element_blank()) +
  ggtitle(label = paste0('Z(x) = ',z.x,'  [p',ifelse(p.x=='<0.001','','='),p.x,']\n','Z(y) = ',z.y,'  [p',ifelse(p.y=='<0.001','','='),p.y,']')) +
  guides(colour = 'none', shape = 'none') +
  annotate(x = 5150, y = 14800, label = '(A)', colour = 'black', geom = 'text', size = 7)



message(paste0(' - circles'))

m1.x <- mean(circles[cluster=='cluster-01',]$x4)
m2.x <- mean(circles[cluster=='cluster-02',]$x4)
sd1.x <- sd(circles[cluster=='cluster-01',]$x4)
sd2.x <- sd(circles[cluster=='cluster-02',]$x4)
print(paste0('CIRCLES - cluster-01: x = ',round(m1.x,2),' +- ',round(sd1.x,2)))
print(paste0('CIRCLES - cluster-02: x = ',round(m2.x,2),' +- ',round(sd2.x,2)))
z.x <- round(abs(m1.x-m2.x)/sqrt(sd1.x^2+sd2.x^2),2)
message(paste0('CIRCLE x - Z score: ',z.x))

m1.y <- mean(circles[cluster=='cluster-01',]$x17)
m2.y <- mean(circles[cluster=='cluster-02',]$x17)
sd1.y <- sd(circles[cluster=='cluster-01',]$x17)
sd2.y <- sd(circles[cluster=='cluster-02',]$x17)
print(paste0('CIRCLES - cluster-01: y = ',round(m1.y,2),' +- ',round(sd1.y,2)))
print(paste0('CIRCLES - cluster-02: y = ',round(m2.y,2),' +- ',round(sd2.y,2)))
z.y <- round(abs(m1.y-m2.y)/sqrt(sd1.y^2+sd2.y^2),2)
message(paste0('CIRCLES y - Z score: ',z.y))

media <- data.table(x = c(m1.x, m2.x), y = c(m1.y, m2.y), stdev.x = c(sd1.x, sd2.x), stdev.y = c(sd1.y, sd2.y), cluster = c('cluster-01', 'cluster-02'))

p.x <- round((1-pnorm(q = z.x))*2,3);p.x <- ifelse(p.x<0.001,'<0.001',as.character(p.x))
p.y <- round((1-pnorm(q = z.y))*2,3);p.y <- ifelse(p.y<0.001,'<0.001',as.character(p.y))

p.circles <- ggplot() +
  geom_point(data = circles, mapping = aes(x = x4, y = x17, colour = cluster, shape = cluster)) +
  geom_point(data = media[cluster=='cluster-01',], mapping = aes(x = x, y = y), colour = 'darkred', shape = 4, stroke = 1.7) +
  geom_point(data = media[cluster=='cluster-02',], mapping = aes(x = x, y = y), colour = 'deepskyblue4', shape = 4, stroke = 1.7) +
  geom_errorbar(data = media[cluster=='cluster-01',], mapping = aes(x = x, ymin = y-stdev.y, ymax = y+stdev.y), colour = 'darkred', width = 0.1) +
  geom_errorbar(data = media[cluster=='cluster-02',], mapping = aes(x = x, ymin = y-stdev.y, ymax = y+stdev.y), colour = 'deepskyblue4', width = 0.1) +
  geom_errorbarh(data = media[cluster=='cluster-01',], mapping = aes(xmin = x-stdev.x, xmax = x+stdev.x, y = y), colour = 'darkred', height = 0.1) +
  geom_errorbarh(data = media[cluster=='cluster-02',], mapping = aes(xmin = x-stdev.x, xmax = x+stdev.x, y = y), colour = 'deepskyblue4', height = 0.1) +
  coord_fixed() +
  scale_x_continuous(limits = c(2050,17050)) +
  scale_y_continuous(limits = c(2050,17050)) +
  theme(axis.text = element_blank()) +
  ggtitle(label = paste0('Z(x) = ',z.x,'  [p',ifelse(p.x=='<0.001','','='),p.x,']\n','Z(y) = ',z.y,'  [p',ifelse(p.y=='<0.001','','='),p.y,']')) +
  guides(colour = 'none', shape = 'none') +
  annotate(x = 2200, y = 16750, label = '(B)', colour = 'black', geom = 'text', size = 7)


p <- grid.arrange(p.clouds, p.circles, nrow = 1)
ggsave(plot = p, filename = paste0(PATH.plots, 'combined.png'), width = 12, height = 8, bg = 'transparent')



